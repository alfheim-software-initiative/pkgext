extern crate ar;

use ar::Archive as deb_ext;
use std::fs::File;
use std::io::copy;
use std::str;

pub fn extract(f: &str) {
    let mut archive = deb_ext::new(File::open(&f).unwrap());
    while let Some(entry_result) = archive.next_entry() {
        let mut entry = entry_result.unwrap();
        let mut file = File::create(
            str::from_utf8(entry.header().identifier()).unwrap(),
            ).unwrap();
        copy(&mut entry, &mut file).unwrap();
    }
}
